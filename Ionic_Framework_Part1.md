# Ionic Framework Part 1

### Developing Ionic HelloWorld App

##### Setting Up Ionic Environment

Before proceed, all the dependencies are require to be installed for running Ionic CLI to initiate a new project.

Requirements for Setup:

* Install [NodeJS](https://nodejs.org/en/download/) & [GIT](https://git-scm.com/downloads)
* Check if npm path is working correctly by the following command
```bash
npm -v
```
* Install Ionic CLI and Cordova CLI with npm using the following command
```bash
npm install -g ionic cordova
```
* Check if Ionic has been installed correctly using following command
```bash
ionic -v
```

##### Developing HelloWorld App

##### *Step 1:*
Execute the `ionic create` command:

`ionic start hello blank`

##### *Step 2:*
Change to `hello` directory by type `cd hello`. The directory structure will be similar like below:

    hello/
     hooks/
     node_modules/
     platforms/
     plugins/
     resources/
     scss/
     www/
     ...
     bower.json
     config.xml
     gulpfile.js
     package.json
     ionic.config.json

 
##### *Step 3:*
If `node_modules` or dependencies installation fail, `npm install` required to be run manually

##### *Step 4:*
Add `Hello World` to the `<ion-content>` directive

```html
<ion-content>
	<h3>Hello World</h3>
</ion-content>
```

##### *Step 5:*
Run `serve` command to run a local web-server to see the output from web browser

`$ ionic serve`

To display comparison between an Android and IOS view, run serve command with `--lab` switch

`$ ionic serve --lab`

---
### Simulate Mobile Devices in Chrome

Use [Chrome DevTools' Device Mode](https://developers.google.com/web/tools/chrome-devtools/device-mode/) to build mobile-first, fully responsive websites. Learn how to use it to simulate a wide range of devices and their capabilities.