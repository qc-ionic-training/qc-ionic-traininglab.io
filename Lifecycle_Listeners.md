# Lifecycle Events Listener

### Remote Debugging

Cordova app will have some visual differences between Chrome Desktop and on phone device. These kind of visual bugs are quite hard to fix, as need to be recompile the app and copy it on your phone every time changes made.

[Chrome Remote Debugging](https://developers.google.com/web/tools/chrome-devtools/remote-debugging/) tool allow remotely debug JavaScript code step by step by simply using Google Chrome.

#### Minimum requirements
1. Cordova 3.3 or higher
2. Android 4.4 or higher
3. Chrome 32 or later installed on your development machine
4. [USB drivers](https://developer.android.com/tools/extras/oem-usb.html) installed on your development machine, if you're using Windows. Ensure Device Manager reports the correct USB driver
5. A USB cable to connect your Android device to your development machine
6. Chrome for Android installed on your Android device
7. [Enabling On-Device Developer Options](https://developer.android.com/studio/run/device.html#developer-device-options)

---
### Listen, Listen, Listen

Based on Session 1 project, follow the steps provided below:

##### *Step 1:*
Add the following code to `index.js`

```javascript
// index.html file
// Wait for device API libraries to load
//
function onLoad() {
    document.addEventListener("deviceready", onDeviceReady, false);
}

// device APIs are available
//
function onDeviceReady() {
    document.addEventListener("pause", onPause, false);
    document.addEventListener("resume", onResume, false);
    document.addEventListener("volumedownbutton", onVolumeDownKeyDown, false);
}

function onPause() {
    console.info('pause event');
}

function onResume() {
    console.info('resume event');
}

function onVolumeDownKeyDown() {
    console.info('volume down event');
}
```


##### *Step 2:*
Build and run the app on device or emulator by using `run` command

`$ cordova run`

##### *Step 3:*
Remotely debug the cordova webview with Chrome from the development machine.
Manually navigate to [chrome://inspect](chrome://inspect) to get to the inspect tab.

![enter image description here](https://gitlab.com/qc-ionic-training/qc-ionic-training.gitlab.io/raw/master/Images/enable-USB-Desktop.jpg)

Connect phone to development machine with USB. On phone device, an alert prompts will be popup to allow USB debugging from development machine. Tap `OK`.

![enter image description here](https://gitlab.com/qc-ionic-training/qc-ionic-training.gitlab.io/raw/master/Images/allow-debugging.png)

##### *Step 4:*
Connected device will be shown on the Chrome debugging list, click `inspect` and remote debugging window will be popup. Test all the the 3 listeners output from the `Console` tab.