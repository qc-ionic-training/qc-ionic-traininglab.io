# Building HelloWorld App

### Dependecies
Before proceed all dependecies for running a Cordova apps need to be installed. Cordova CLI then will be used to initiate a new project.

##### *Step 1:*
Download [NodeJS](http://nodejs.org/) & [NPM](https://npmjs.org/package/cordova) for managing dependencies.

##### *Step 2:*
Open command promt or Terminal, and type

`$ npm install -g cordova`

---

### Create HelloWorld project
Once Cordova dependecies are installed. Proceed with building an example app using Cordova CLI.

##### *Step 1:*
Execute the cordova create command on CommandPrompt(Windows) & Terminal(MAC OS)

`$ cordova create hello com.example.hello HelloWorld`

##### *Step 2:*
Check the Cordova Help command to see any other commands for Cordova Project.

`$ cordova help`

##### *Step 3:*
Change directory to hello by type `cd hello` and then, Add a Platform to the cordova project accordingly.

```bash
 $ cd hello   
 $ cordova platform add ios --save   
 $ cordova platform add android@^5.0.0 --save   
```

Installed platforms can be check by running the following command:

`$ cordova platform list`

##### *Step 4:*
Test if the cordova app is able to prepare itself, resolve all dependencies etc. by run:

`$ cordova prepare`

##### *Step 5:*
Emulate the app for all platforms or specific installed platform. Running following command will open installed Device Emulation either based on installed SDK or 3rd-party emulator like [GenyMotion](https://www.genymotion.com/)

`$ cordova emulate ios`

##### *Step 6:*
One of the best feature of hybrid app during development is being able to preview the app output simply in modern web browser like Google Chrome without compile and run it to real device. Use `cordova serve` to host the app on a local dev web server and browse it on the browser during development:

`$ cordova serve`

##### *Step 7:*
In order to compile and generate builds, run the `compile` command for a specific platform or without any platform to compile for all.

`$ cordova compile`

##### *Step 8:*
Execute `run` command to prepare, build and run the app to available device (Notes: Android required [Developer Options and USB Debugging enabled](https://developer.android.com/studio/debug/dev-options.html)). It will try to find a device connected to the machine for running; else will deploy to any available emulator.

`$ cordova run`