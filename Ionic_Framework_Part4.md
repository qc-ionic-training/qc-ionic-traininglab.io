# Ionic Framework Part 4

### Build Service Using $http

##### Using Third Party API
Visit API webpage from https://api.jomgeek.com, you can create a new account or just using this pre-registered account to login:
```
// j0mgeek
// password
```

Create a new API key, or Skip this step if you already have one. 
Link: https://api.jomgeek.com/myapi

Open "RainFall" menu. Link: https://api.jomgeek.com/module/rainfall

Copy the API Url, record it along with the given http method for further instruction

Modify `rainfall.service.js` by creating a Http Request using AngularJs `$http` and `$q` according to copied API Url:
 
```javascript
angular
    .module('rainfall.module')
    .factory('RainFallService', function($http, $q) {
        return {
            getAllRecords: function () {
                return $q(function (resolve, reject) {
                    $http.get('<--API URL--->').then(function (res) {

                        if ( res.status === 200 ) {
                            resolve(res.data.data);
                        } else {
                            reject('Service Failure: ' + res.status);
                        }
                    }).catch(function (err) {
                        reject('Internal Error: ' + err);
                    });
                });
            }
        }
    })
;
``` 

The service is now ready to be use by `RainFallController`, simply by injecting the service to the dependencies, and storing the value to `$scope` for **two-way-databinding** with `rainfall.html`

```javascript
angular
    .module('rainfall.module')
    .controller('RainFallController', function($scope, RainFallService) {
        RainFallService.getAllRecords().then(function (res) {
            $scope.datasets = res;
        })
    })
;
```

Modify the view by adding `ng-repeat` AngularJs looping directive and display the current scope data.
```html
<ion-view view-title="Rainfall">
    <ion-content>
        <div class="card" ng-repeat="data in datasets">
            <div class="item item-text-wrap">
                <div class="row">
                    <div class="col col-75">
                        {{ data.station }}
                    </div>
                    <div class="col">
                        {{ data.reading }} mm
                    </div>
                </div>
            </div>
        </div>
    </ion-content>
</ion-view>
```

#### Gotcha
If you facing an issue try to open the Chrome Dev tool and debug your error. If you're facing `No 'Access-Control-Allow-Origin'` issue most probably because you're running the codebase on localhost. To short-fix this issue simply by downloading [CORS Toggle Chrome plugin](https://chrome.google.com/webstore/detail/cors-toggle/omcncfnpmcabckcddookmnajignpffnh?hl=en)