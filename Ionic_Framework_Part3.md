# Ionic Framework Part 3

### Adding UI Card Component

##### Duplicate Component Template

Assume that you've already created `RoomList` module in component way (best practice), duplicate the whole directory and renamed it to `RailFallList`, the whole structure will look likely below:
```html
www/
 RoomList/.
 RainFallList/
  rainfall.config.js
  rainfall.controller.js
  rainfall.html
  rainfall.module.js
  rainfall.service.js
```

And don't forget to **change the module name, controller, service, routing** and add new tab container in `tabs.html` as below:

```html
<!-- www/templates/tabs.html -->

<!-- RainFall Tab -->
<ion-tab title="Rooms" icon-off="ion-ios-rainy-outline" icon-on="ion-ios-rainy" ui-sref="tab.rainfall-list">
  <ion-nav-view name="tab-rainfall"></ion-nav-view>
</ion-tab>
```
Include all the `.js` files reference at `index.html`
```html
<!-- www/index.html -->

<!-- RainFall -->
<script src="js/RainFallList/rainfall.module.js"></script>
<script src="js/RainFallList/rainfall.controller.js"></script>
<script src="js/RainFallList/rainfall.service.js"></script>
<script src="js/RainFallList/rainfall.config.js"></script>
```

And finally add the `rainfall.module` in `app.js` main project dependecies
```javascript
// www/app.js

angular.module('starter', ['ionic', 'roomlist.module', 'rainfall.module'])
```

Execute command `serve` and observe the output produced
`$ ionic serve`

---
##### Add Card Component

**Official Docs:** http://ionicframework.com/docs/v1/components/#cards
**Reference:** http://ionicframework.com/docs/v1/components/#grid

Modify `rainfall.html` and add ionic card component with modified grid into `<ion-content>` as below

```html
<!-- www/js/RainFallList/rainfall.html -->

<ion-content>
   <div class="card">
      <div class="item item-text-wrap">
         <div class="row">
            <div class="col col-75">
               Taiping
            </div>
            <div class="col">
               20 mm
            </div>
         </div>
      </div>
   </div>
</ion-content>
```

Observe the output from `ionic serve` local server

### Adding UI List Component

Modify `roomlist.html` and add ionic list component into `<ion-content>` as below

```html
<!-- www/js/RoomList/roomlist.html -->

<ion-view view-title="Room List">
    <ion-content>
        <div class="list">
            <a class="item item-thumbnail-left" href="#">
                <img src="http://lorempixel.com/350/350/city/">
                <h2>Random Hotel</h2>
                <p>Seberang Perai, Penang</p>
            </a>
        </div>
    </ion-content>
</ion-view>

```

Observe the output from `ionic serve` local server