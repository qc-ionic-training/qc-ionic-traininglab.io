# Ionic Framework Part 2

### Structuring Ionic Project

```javascript
// ---- Notes ----
// running command `ionic serve` or `ionic serve --lab` help you to watch any errors during code
//
// ---- Gotcha ---
// Add the following gulp task to gulpfile.js to fix issue regarding watching SASS changes
// => gulpfile.js
// gulp.task('serve:before', ['sass', 'watch']);
//------
```

##### Directory Best Practice

In order to have a better maintainability code, it is encourage to developer to construct a good files structure of the ionic app.

One of the biggest important thing is to define 1 component per file, for example:

```html
app/  
    detail/
        detail.controller.js
        detail.html
    overview/
        overview.controller.js
        overview.service.js
        overview.html
    services/
        service1.js
        service2.js
    settings/
        settings.controller.js
        settings.html
```

##### Getting Started

##### *Step 1:*

Create a new ionic project with tabs starter template, name the project as bookingapp

`ionic start -i my.psp.bookingapp ./bookingapp tabs`

##### *Step 2:*

Find the best name for the specific component or child component, for example `RoomList`.

##### *Step 3:*

After then, create a directory in `www/js/RoomList`

##### *Step 4:*

In `www/js/RoomList/` directory, create a module file `roomlist.module.js`

```javascript
// www/js/RoomList/roomlist.module.js

'use strict';

angular.module('roomlist.module', [

    'ui.router'
]);
```

##### *Step 5:*

Followed by the independent routing and config, named `roomlist.config.js`

```javascript
// www/js/RoomList/roomlist.config.js

'use strict'

angular
    .module('roomlist.module')
    .config(function config($stateProvider) {
        $stateProvider

            .state('tab.room-list', {
                url: '/roomList',
                views: {
                    'tab-rooms': {
                        templateUrl: 'js/RoomList/roomlist.html',
                        controller: 'RoomListController'
                    }
                }
            });
    })
;
```

##### *Step 6:*

Create controller for the route/state, name it as `roomlist.controller.js`

```javascript
// www/js/RoomList/roomlist.controller.js

'use strict';

angular
    .module('roomlist.module')
    .controller('RoomListController', function($scope) {
        // do anything
    })
;
```

##### *Step 7:*

Create a simple view for the controller, name it `roomlist.html`

```html
<!-- www/js/RoomList/roomlist.html -->

<ion-view view-title="Room List">
    <ion-content class="padding">
        <h2>Meeting Room List</h2>
    </ion-content>
</ion-view>
```

##### *Step 8:*

Finally, create a service for the controller, if necessary. Named it as `roomlist.service.js`

```javascript
// www/js/RoomList/roomlist.service.js

'use strict';

angular
    .module('roomlist.module')
    .factory('RoomListService', function() {
        // Might use a resource here that returns a JSON array
    })
;
```

##### *Step 9:*

Now that you've created the;

```
roomlist.module.js
roomlist.controller.js
roomlist.service
roomlist.config.js
roomlist.html
```

Include all the in `index.html` right before `</head>` close tag

```html
<!-- www/index.html -->

<!-- RoomList -->
<script src="js/RoomList/roomlist.module.js"></script>
<script src="js/RoomList/roomlist.controller.js"></script>
<script src="js/RoomList/roomlist.service.js"></script>
<script src="js/RoomList/roomlist.config.js"></script>
```

##### *Step 10:*

To make the AngularJs read those created Model-View-Controller stack files, include `roomlist.module` inside of primary module

```javascript
// www/js/app.js

angular.module('starter', ['ionic', 'roomlist.module'])
```

##### *Step 11:*

As we use `tab-rooms` in our state router in **Step 5**, we should modify `tabs.html` to add a new tab container with `tab-rooms` name.

```html
<!-- www/templates/tabs.html -->

<!-- Rooms Tab -->
<ion-tab title="Rooms" icon-off="ion-ios-people-outline" icon-on="ion-ios-people" ui-sref="tab.room-list">
    <ion-nav-view name="tab-rooms"></ion-nav-view>
</ion-tab>
```

---

**Benefit:** Instead of including all RoomList declaration like `RoomListService` and `RoomListController`, we just need to include the primary declaration of `roomlist.module`. Thus increase the efficiency of code maintenance and best practice.

##### *Additional Step:*

Remove all sample controller, service, and dependecies that unrelated; coming from the starter template.