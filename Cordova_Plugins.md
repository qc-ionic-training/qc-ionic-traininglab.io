# Cordova Plugins

### Icon and SplashScreen

A Mobile App requires static resources such as icons and splash screens. Each platform has specialised requirements regarding the sizes and format for icons.

#### Icon and Splash Screen Online Generator

[Online PhoneGap File Generator](http://phonegap.appiq.software/) allow these static files generated automatically based on single file input for Icon and SplashScreen.

##### Minimum specification
1. Use .png files for best result
2. Recommended minimal resolution for icon is 1024px x 1024px
    Example File: [Icon-Example.png](https://gitlab.com/qc-ionic-training/qc-ionic-training.gitlab.io/raw/master/Images/icon-example-psp.png)
3. Recommended minimal resolution for splash is 2208px x 2208px
    Example File: [Splash-Example.png](https://gitlab.com/qc-ionic-training/qc-ionic-training.gitlab.io/raw/master/Images/splash-example-psp.png)

#### Adding Static Images Resource

##### *Step 1:*
Generate and download resources from [Online PhoneGap File Generator](http://phonegap.appiq.software/)  

##### *Step 2:*
Extract zip to root path of Cordova project. The directory structure will be similar like below:

    /
     hooks/
     platforms/
     plugins/
     res/
     www/
     config.xml
     icon.png
     splash.png

##### *Step 3:*
Modify `config.xml` according to this:

```xml
<platform name="android">
  <!-- Icon !-->
  <icon src="icon.png" />
  <icon density="ldpi" src="res/icons/android/icon-36-ldpi.png" />
  <icon density="mdpi" src="res/icons/android/icon-48-mdpi.png" />
  <icon density="hdpi" src="res/icons/android/icon-72-hdpi.png" />
  <icon density="xhdpi" src="res/icons/android/icon-96-xhdpi.png" />
  <icon density="xxhdpi" src="res/icons/android/icon-144-xxhdpi.png" />
  <icon density="xxxhdpi" src="res/icons/android/icon-192-xxxhdpi.png" />

  <!-- SplashScreen !-->
  <splash src="splash.png" />
  <splash density="land-hdpi" src="res/screens/android/screen-hdpi-landscape.png" />
  <splash density="land-ldpi" src="res/screens/android/screen-ldpi-landscape.png" />
  <splash density="land-mdpi" src="res/screens/android/screen-mdpi-landscape.png" />
  <splash density="land-xhdpi" src="res/screens/android/screen-xhdpi-landscape.png" />
  <splash density="port-hdpi" src="res/screens/android/screen-hdpi-portrait.png" />
  <splash density="port-ldpi" src="res/screens/android/screen-ldpi-portrait.png" />
  <splash density="port-mdpi" src="res/screens/android/screen-mdpi-portrait.png" />
  <splash density="port-xhdpi" src="res/screens/android/screen-xhdpi-portrait.png" />
</platform>
```

##### *Step 4:*
Add **Cordova SplashScreen Plugin** by run the following command

`$ cordova plugin add cordova-plugin-splashscreen --save`

##### *Step 5:*
Build and run the app on device or emulator by using `run` command

`$ cordova run`

---
### Cordova Network Information

This plugin provides an implementation of an old version of the Network Information API. It provides information about the device's cellular and wifi connection, and whether the device has an internet connection.

##### *Step 1:*
Create `checkConnection()` function inside  `index.js` by add the following code:

```javascript
function checkConnection() {
    var networkState = navigator.connection.type;

    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.CELL]     = 'Cell generic connection';
    states[Connection.NONE]     = 'No network connection';

    alert('Connection type: ' + states[networkState]);
}
```

##### *Step 2:*

Add a button to `index.html`

```html
<button id="btnCheckConnection">Check Connection</button>
```

##### *Step 3:*

Register click event during `deviceready` for the button to point to `checkConnection()`

```javascript
onDeviceReady: function() {
 ...
 document.getElementById("btnCheckConnection").addEventListener("click", checkConnection);
}
```

##### *Step 4:*
Build and run the app on device or emulator by using `run` command

`$ cordova run`