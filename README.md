# Cordova Ionic Training

Hands-on training provided by [QalamCreative](http://qalamcreative.com)

Original author is [Afif](mailto://afif02@gmail.com)

## Prerequisite

* These tutorials are intended for starters in building hybrid app using Cordova and Ionic
* Follow the full guide on [https://qc-ionic-training.gitlab.io](https://qc-ionic-training.gitlab.io)

## License (MIT)

These tutorials and source-code are published under the [MIT License](https://gitlab.com/qc-ionic-training/qc-ionic-training.gitlab.io/blob/master/LICENSE.md)
which allows very broad use for both academic and commercial purposes.

A few of the images used for demonstration purposes may be under copyright. These images are included under the "fair usage" laws.

You are very welcome to modify these tutorials and use them in your own projects.
Please keep a link to the [original repository](https://gitlab.com/qc-ionic-training).